<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class All extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        

        

        

        Schema::create('available_infrastructures', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('name')->unique();
                    
                    $table->integer('points')->unsigned();
                    


                    $table->timestamps();

                    
                    
        });
        

        



        

        

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agencies');
        Schema::drop('agents');
        Schema::drop('vehicles');
        Schema::drop('infrastructures');
        Schema::drop('available_infrastructures');
        Schema::drop('laboratories');
    }
}

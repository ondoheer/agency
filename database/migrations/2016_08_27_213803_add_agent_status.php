<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgentStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_status', function($table){
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();

        });
        Schema::table('agents', function ($table) {
           
            $table->integer('status_id')->unsigned()->nullable();
            $table->foreign('status_id')
                        ->references('id')
                        ->on('agent_status');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_status');
        Schema::table('agents', function($table){
            $table->dropColumn('status_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyParts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('foundation_year')->unsigned();
            $table->integer('research_points')->unsigned();
            $table->integer('infrastructure_points')->unsigned();
            $table->integer('agent_points')->unsigned();            
            $table->integer('information_points')->unsigned();
            
            $table->timestamps();                            
                    
        });

        Schema::create('offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('location');
            $table->date('opened_on');
            

            $table->timestamps();

            $table->integer('agency_id')->unsigned();
            $table->foreign('agency_id')
                        ->references('id')
                        ->on('agencies')
                        ->onDelete('cascade');
            $table->integer('office_type_id')->unsigned();
            $table->foreign('office_type_id')
                ->references('id')
                ->on('office_types');
        });

        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name')->unique();
            $table->string('occupation')->nullable();
            $table->integer('age')->unsigned()->nullable();
            $table->string('image')->default('portrait.jpg')->nullable();
            $table->integer('points')->unsigned();
            
            $table->timestamps();

            $table->integer('agency_id')->unsigned();
            $table->foreign('agency_id')
                        ->references('id')
                        ->on('agencies')
                        ->onDelete('cascade');
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->integer('agent_type_id')->unsigned();
            $table->foreign('agent_type_id')
                    ->references('id')
                    ->on('agent_types');
        });

        

        Schema::create('infrastructures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            
                       
            
            $table->date('acquired_on');

            $table->integer('office_id')->unsigned();
            $table->foreign('office_id')
                ->references('id')
                ->on('offices');

            $table->integer('infrastructure_type_id')->unsigned();
                $table->foreign('infrastructure_type_id')
                    ->references('id')
                    ->on('infrastructure_types');
                    
            $table->timestamps();

                    
                    
        });

        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('infrastructures');        
        Schema::drop('agents');
        Schema::drop('offices');
        Schema::drop('agencies');
    }
}

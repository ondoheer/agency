<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_types', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug');
                $table->text('description')->nullable();
                $table->integer('points')->unsigned();
                $table->timestamps();

                            
        });
        Schema::create('infrastructure_types', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug');
                $table->text('description')->nullable();
                $table->integer('points')->unsigned();
                $table->timestamps();

                
        });
        Schema::create('office_types', function($table){
           $table->increments('id');
           $table->string('name')->unique();
           $table->string('slug')->unique();
           $table->integer('points')->unsigned();
           $table->string('description')->nullable();
           $table->timestamps();

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('office_types');
        Schema::drop('agent_types');
        Schema::drop('infrastructure_types');

        
    }
}

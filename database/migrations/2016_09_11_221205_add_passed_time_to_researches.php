<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPassedTimeToResearches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('researches', function($table){
            $table->integer('passed_time')->nullable()->unsigned()->defaults(0);
            $table->dropColumn('percentage_completed');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('researches', function($table){
            $table->dropColumn('passed_time');            
            $table->integer('percentage_completed')->unsigned()->defaults(0);
            
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('researches', function($table){
             $table->increments('id');
             $table->string('name')->unique();
             $table->string('slug')->unique();
             $table->text('description');
             $table->integer('required_time')->unsigned();
             $table->integer('percentage_completed')->unsigned()->defaults(0);             
             $table->integer('points')->unsigned();

             $table->integer('lab_id')->unsigned()->nullable();
             $table->foreign('lab_id')
                ->references('id')
                ->on('infrastructures');

            $table->integer('required_lab_id')->unsigned();
            $table->foreign('required_lab_id')
                ->references('id')
                ->on('infrastructure_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('researches');
    }
}

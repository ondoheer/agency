<?php

use Illuminate\Database\Seeder;

class OfficeTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('office_types')->insert([
        	[
        		'name'=>'Small Base',
        		'slug'=>'small_base',
        		'description'=>'Small Base',
        		'points'=> '10'
        	],
        	[
        		'name'=>'Medium Base',
        		'slug'=>'medium_base',
        		'description'=>'Medium Base',
        		'points'=> '25'
        	],
        	[
        		'name'=>'Large Base',
        		'slug'=>'large_base',
        		'description'=>'Large Base',
        		'points'=> '50'
        	],
        ]);
    }
}

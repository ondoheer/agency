<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
    	$this->call(AgentTypeSeeder::class);
        
        $this->call(InfrastructureTypeSeeder::class);
        $this->call(AgentStatusSeeder::class);
        $this->call(OfficeTypesSeeder::class);
        
        Model::reguard();
    }
}

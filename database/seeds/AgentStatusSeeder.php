<?php

use Illuminate\Database\Seeder;

class AgentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agent_status')->insert([
        	[
        		"name"=>"Active",
        		"slug"=>"active"
        	],
        	[
        		"name"=>"R.I.P",
        		"slug"=>"rip"
        	],
        	[
        		"name"=>"Injured",
        		"slug"=>"injured"
        	],
        	[
        		"name"=>"M.I.A.",
        		"slug"=>"mia"
        	],

    	]);
    }
}

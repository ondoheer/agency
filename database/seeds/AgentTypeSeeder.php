<?php

use Illuminate\Database\Seeder;

class AgentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agent_types')->insert([
                	[
                		'name'=>'Field Combat Agent Rookie',
                		'slug'=>'field_combat_agent_rookie',
                		'description'=>'Barely trained, freshman',
                		'points'=> '1'
                	],
                	[
                		'name'=>'Field Combat Agent Veteran',
                		'slug'=>'field_combat_agent_veteran',
                		'description'=>'Knows Combat and can handle stress',
                		'points'=> '4'
                	],[
                		'name'=>'Field Combat Agent Expert',
                		'slug'=>'field_combat_agent_expert',
                		'description'=>'Survivor of many encounters, fit to lead.',
                		'points'=> '10'
                	],
                	[
                		'name'=>'Field investigator Agent Rookie',
                		'slug'=>'field_investigator_agent_rookie',
                		'description'=>'Barely trained, freshman',
                		'points'=> '2'
                	],
                	[
                		'name'=>'Field investigator Agent Veteran',
                		'slug'=>'field_investigator_agent_veteran',
                		'description'=>'has handled some cases and can handle stress',
                		'points'=> '5'
                	],[
                		'name'=>'Field investigator Agent Expert',
                		'slug'=>'field_investigator_agent_expert',
                		'description'=>'Survivor of many encounters, fit to lead.',
                		'points'=> '12'
                	],
                	[
                		'name'=>'Field Medic Agent Rookie',
                		'slug'=>'field_medic_agent_rookie',
                		'description'=>'Barely trained, freshman',
                		'points'=> '1'
                	],
                	[
                		'name'=>'Field Medic Agent Veteran',
                		'slug'=>'field_medic_agent_veteran',
                		'description'=>'Knows combat medicine and can handle stress',
                		'points'=> '4'
                	],[
                		'name'=>'Field Medic Agent Expert',
                		'slug'=>'field_medic_agent_expert',
                		'description'=>'Survivor of many encounters, fit to lead.',
                		'points'=> '10'
                	],
                	[
                		'name'=>'Specialist Agent Rookie',
                		'slug'=>'specialist_agent_rookie',
                		'description'=>'Barely trained, freshman Knows about 1-2 subjects',
                		'points'=> '2'
                	],
                	[
                		'name'=>'Specialist Agent Veteran',
                		'slug'=>'specialist_agent_veteran',
                		'description'=>'Knows Combat and can handle stress, knows 3-4 subjects',
                		'points'=> '7'
                	],[
                		'name'=>'Specialist Agent Expert',
                		'slug'=>'specialist_agent_expert',
                		'description'=>'Survivor of many encounters, fit to lead, knows 5+ subjects',
                		'points'=> '13'
                	],
                	[
                		'name'=>'Researcher Rookie',
                		'slug'=>'researcher_rookie',
                		'description'=>'Just out of college, freshman',
                		'points'=> '2'
                	],
                	[
                		'name'=>'Researcher Veteran',
                		'slug'=>'researcher_veteran',
                		'description'=>'Has lead an experiment at least one',
                		'points'=> '5'
                	],[
                		'name'=>'Researcher Expert',
                		'slug'=>'researcher_expert',
                		'description'=>'Published at least 2 papers on exciting reseach fields.',
                		'points'=> '12'
                	],
                	[
                		'name'=>'Special Agent (powers)',
                		'slug'=>'special_agent',
                		'description'=>'Has a natural ability, telepathy, telekinesis, seas dead people, etc.',
                		'points'=> '15'
                	],
    

                ]);
    }
}

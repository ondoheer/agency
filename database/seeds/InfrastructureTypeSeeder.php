<?php

use Illuminate\Database\Seeder;

class InfrastructureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infrastructure_types')->insert([
                	[
                		'name'=>'Small Inter Agency Office',
                		'slug'=>'small_ia_office',
                		'description'=>'Small Inter Agency Office',
                		'points'=> '3'
                	],
                	[
                		'name'=>'Medium Inter Agency Office',
                		'slug'=>'medium_ia_office',
                		'description'=>'Medium Inter Agency Office',
                		'points'=> '7'
                	],
                	[
                		'name'=>'Large Inter Agency Office',
                		'slug'=>'large_ia_office',
                		'description'=>'Large Inter Agency Office',
                		'points'=> '12'
                	],
                	
                	[
                		'name'=>'Small Comm Station',
                		'slug'=>'small_comm_station',
                		'description'=>'Small Comm. Station',
                		'points'=> '3'
                	],
                	[
                		'name'=>'Medium Comm Station',
                		'slug'=>'medium_comm_station',
                		'description'=>'Medium Comm. Station',
                		'points'=> '7'
                	],
                	[
                		'name'=>'Large Comm Station',
                		'slug'=>'large_comm_station',
                		'description'=>'Large Comm. Station',
                		'points'=> '12'
                	],
                	[
                		'name'=>'Small Imprisonment Facility',
                		'slug'=>'small_imprisonment',
                		'description'=>'Small Imprisonment',
                		'points'=> '4'
                	],
                	[
                		'name'=>'Medium Imprisonment Facility',
                		'slug'=>'medium_imprisonment',
                		'description'=>'Medium Imprisonment',
                		'points'=> '12'
                	],
                	[
                		'name'=>'Large Imprisonment Facility',
                		'slug'=>'large_imprisonment',
                		'description'=>'Large Imprisonment',
                		'points'=> '40'
                	],
                	[
                		'name'=>'Small Safe House',
                		'slug'=>'small_safe_house',
                		'description'=>'Small House',
                		'points'=> '2'
                	],
                	[
                		'name'=>'Medium Safe House',
                		'slug'=>'medium_safe_house',
                		'description'=>'Medium House',
                		'points'=> '5'
                	],
                    [
                        'name'=>'Small Lab',
                        'slug'=>'small_lab',
                        'description'=>'Small Laboratory, apt for upt to 4 scientist and 1 small project',
                        'points'=> '10'
                    ],
                    [
                        'name'=>'Medium Lab',
                        'slug'=>'medium_lab',
                        'description'=>'Medium Laboratory, apt for upt to 10 scientist and 3 small or 1 medium projects',
                        'points'=> '30'
                    ],
                    [
                        'name'=>'Large Lab',
                        'slug'=>'large_lab',
                        'description'=>'Large Laboratory, apt for upt to 40 scientist and 1 large, 4 medium or 12 small projects',
                        'points'=> '70'
                    ],


                ]);
    }
}

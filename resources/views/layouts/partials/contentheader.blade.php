<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @if($agency)
        {{$title or $agency->name}}
        <br>
        
        <b>Current Date: </b>{{$subtitle or $agency->current_date}}
        @endif
    </h1>
    {{-- @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
    @if (Session::has('status'))
    	<div class="alert alert-success">
    		<b>{{ Session::get('status') }}</b>
    	</div>
    @endif
    
</section>
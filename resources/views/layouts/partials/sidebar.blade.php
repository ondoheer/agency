<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->
           
            <li><a href="{{route('agency')}}"><i class='fa fa-shield'></i> <span>Overview</span></a></li>
           
            <li><a href="{{route('offices')}}"><i class='fa fa-hospital-o'></i> <span>Offices</span> </a></li>
            <li><a href="{{route('manage_agents')}}"><i class='fa fa-user'></i> <span>Agents</span> </a></li>
            <li><a href="{{route('researches')}}"><i class='fa fa-flask'></i> <span>Research</span> </a></li>
             <li><a href="{{route('npcs')}}"><i class='fa fa-binoculars'></i> <span>Information/Logistics</span> </a></li>
             <li><a href="{{route('public_worldevents')}}"><i class='fa fa-globe'></i> <span>World Events</span> </a></li>
            <li><a href="{{route('missions')}}"><i class='fa fa-book'></i> <span>Missions & Reports</span> </a></li>
            <li><a href="{{route('npcs')}}"><i class='fa fa-user-md'></i> <span>NPCs</span> </a></li>

        @if(Auth::check())
             @if(Auth::user()->email == "ondoheer@gmail.com")
                <li class="header">Admin Menu</li>
                 <li><a href="{{route('agency.manage')}}"><i class='fa fa-building'></i> <span>Manage Agency</span> </a></li>
                 <li><a href="{{route('agents_admin')}}"><i class='fa fa-user'></i> <span>Manage Agents</span> </a></li>
                 <li><a href="{{route('research.create')}}"><i class='fa fa-flask'></i> <span>Manage Researches</span> </a></li>
                 <li><a href="{{route('worldevents')}}"><i class='fa fa-globe'></i> <span>World Events</span> </a></li>
                <li><a href="{{route('types')}}"><i class='fa fa-gear'></i> <span>Types (App Data)</span> </a></li>
            @endif
        @endif
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

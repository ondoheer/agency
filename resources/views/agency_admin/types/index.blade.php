@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	
		<div class="row">
			<div class="col-md-6 ">
				<div class="panel panel-default">
					<div class="panel-heading">Type Creator</div>
					<div class="panel-body">
					<p>Select the type of item you want to add to the system choices</p>
					</div>

					<div class="panel-body">
						<form method="POST" action="{{route('types.create')}}">
							<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">

							<label for="type">Data type</label>
							<select class="form-control" name="type" id="type">
								<option value="agent">Agent</option>
								<option value="infrastructure">Infrastructure</option>
							</select>

							<label for="name">Type Name</label>
							<input class="form-control" type="text" id="name", name="name">
							
							<label for="points">Cost in points</label>
							<input class="form-control" type="integer" name="points" id="points">
							<label for="description">Description</label>
							<input class="form-control" type="text" name="description" id="description">

							<input class="btn btn-success right" type="submit">
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6 ">
				<div class="panel panel-default">
					
						<div class="panel-heading">Agent Types</div>
						<div class="panel-body">
							<table>
								<tr>
									<th>Name</th>
									<th>Cost</th>
									<th>Description</th>
								</tr>
								@foreach($agents as $agent)
								<tr>
									<td>{{$agent->name}}</td>
									<td>{{$agent->points}}</td>
									<td>{{$agent->description}}</td>
								</tr>
								@endforeach
							</table>				

						</div>
				</div>
				
				<div class="panel panel-default">
					
					<div class="panel-heading">Infrastructure Types</div>
					<div class="panel-body">
						<table>
							<tr>
								<th>Name</th>
								<th>Cost</th>
								<th>Description</th>
							</tr>
							@foreach($infrastructures as $infrastructure)
							<tr>
								<td>{{$infrastructure->name}}</td>
								<td>{{$infrastructure->points}}</td>
								<td>{{$infrastructure->description}}</td>
							</tr>
							@endforeach
									</table>				

								</div>
						</div>
					</div>
		</div>
		
	</div>

	
		
	
			
			
		</div>
	</div>

@endsection
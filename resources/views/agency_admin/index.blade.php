@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
<div class="container spark-screen">
		<div class="row">
			<div class="col-md-9 ">
				<div class="panel panel-default">
					<div class="panel-heading">Agents to User Assignment
					</div>
					<div class="panel-body">
						<div class="row">
							@foreach($agents as $agent)
								<div class="panel panel-default col-md-3">
									<div class="panel-heading">
										<h5>{{$agent->name}}</h5>

									</div>
									<div class="panel-body">
										<div>Occ:{{$agent->occupation}}</div>
										<div>Age: {{$agent->age}}</div>

										@if($agent->type)
										<div>Type: {{$agent->type->name}}</div>
										@endif
										@if($agent->player)
									<div>Player: {{$agent->player->name}}</div>
									@endif

									<div>Assign to:</div>
										 
										<form method="POST" action="{{route('agent_to_user')}}">
									
									<input type="hidden" value="{{$agent->id}}" name="agent">
									<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
									<select name="user" id="user-{{$agent->id}}">
										
										@foreach($users as $user)
										<option value="{{$user->id}}">{{$user->name}}</option>
										@endforeach
									</select>
									<input value="&#10004;" type="submit" class="btn btn-tiny btn-success">
									
								</form>
									</div>
								</div>
								
									
								
							@endforeach
						</div>
							
					</div>
				</div>
			</div>
			<div class="col-md-3 ">
				<div class="panel panel-default">
					<div class="panel-heading">Create Agents
					</div>
					<div class="panel-body">
						<form method="POST" action="{{route('create_agent')}}">
							<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
							<label for="name">
								Name
							</label>
							<input class="form-control" type="text" id="name" name="name">
							<label for="age">
								Age
							</label>
							<input class="form-control" type="integer" id="age" name="age">
							<label for="occupation">
								Occupation
							</label>
							<input class="form-control" type="text" id="occupation" name="occupation">

							<label  for="type">Type</label>
							<select class="form-control" name="type" id="type">
								@foreach($agent_types as $type)
									<option value="{{$type->id}}">{{$type->name}}</option>
								@endforeach
							</select>

							<input type="submit" class="btn btn-success">

						</form>
					

					</div>
				</div>
			</div>
		</div>
	</div>

	@if(count($errors) > 0)
		<ul>
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{$error}}</li>
			@endforeach
		</ul>
	@endif
@endsection
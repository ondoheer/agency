<div class="panel panel-default">
	<div class="panel-heading">Update Stats</div>
	

	<div class="panel-body">
		@if(!$agency)
			<p>You've got to create an agency before being able to update it's remaining stat points</p>
		@else
		<form method="POST" action="{{route('agency.update_points')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="research_points">Resarch Points</label>
			<input class="form-control" value="{{$agency->research_points or 0}}" type="integer" name="research_points", id="research_points">

			<label for="infrastructure_points">Infrastructure Points</label>
			<input class="form-control" value="{{$agency->infrastructure_points or 0}}" type="integer" name="infrastructure_points", id="infrastructure_points">
			
			<label for="agent_points">Agent Points</label>
			<input class="form-control" value="{{$agency->agent_points or 0 }}" type="integer" name="agent_points", id="agent_points">

			<label for="information_points">Information Points</label>
			<input class="form-control" value="{{$agency->information_points or 0}}" type="integer" name="information_points", id="information_points">

			


			<input class="btn btn-success" type="submit">
		</form>
		@endif
	</div>
</div>
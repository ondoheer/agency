<div class="panel panel-default">
	<div class="panel-heading">Agency</div>
	

	<div class="panel-body">
	@if($agency)
		<h4>{{$agency->name}}</h4>
		<p>{{$agency->foundation_year}}</p>
	@else
		<form action="{{route("agency.store")}}" method="POST">
			
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">

			<label for="name">Agency Name</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="foundation">Foundation year</label>
			<input class="form-control" type="integer" name="foundation", id="foundation">

			<input class="btn btn-default" type="submit">
			

		</form>
	@endif
	</div>
</div>
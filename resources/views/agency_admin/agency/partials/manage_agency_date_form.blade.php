<div class="panel panel-default">
	<div class="panel-heading">Update Date</div>
	

	<div class="panel-body">
		@if(!$agency)
			<p>You've got to create an agency before being able to manage it's dates</p>
		@else
		<form method="POST" action="{{route('agency.update_date')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<h4>Current Date</h4>
			<h5>{{$agency->current_date or "01-01-" . $agency->foundation_year}}</h5>

			<label for="days">Days to advance</label>
			<input class="form-control" value="0" type="integer" name="days", id="days">

			<br>

			


			<input class="btn btn-success" type="submit" value="Update">
		</form>
		@endif
	</div>
</div>
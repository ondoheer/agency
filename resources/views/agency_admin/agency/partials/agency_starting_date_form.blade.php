<div class="panel panel-default">
	<div class="panel-heading">Starting Date</div>
	

	<div class="panel-body">
		@if(!$agency || !$agency->foundation_year )
			<p>You've got to create an agency and  set it's foundation year before being able set a gaming starting date</p>

		
		
		@else
		<form method="POST" action="{{route('agency.start_date')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<label for="start_date">Game Starting Date (on gaming time)</label>
			<input class="form-control" value="0" type="date" name="start_date", id="start_date">

			<br>

			


			<input class="btn btn-success" type="submit" value="Update">
		</form>
		@endif
	</div>
</div>
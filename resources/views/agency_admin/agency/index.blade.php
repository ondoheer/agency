@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection
@section('main-content')
	
<div class="row">
	<div class="col-md-2 ">
		@include('agency_admin.agency.partials.create_show_agency_form')
	</div>
	<div class="col-md-2 ">
		@include('agency_admin.agency.partials.agency_stats_form')
	</div>
	
	@if(!$agency || !$agency->starting_date)
		<div class="col-md-4 ">
			@include('agency_admin.agency.partials.agency_starting_date_form')
		</div>
	@else
		<div class="col-md-4 ">
			@include('agency_admin.agency.partials.manage_agency_date_form')
		</div>
	@endif
	
</div>
				
@endsection
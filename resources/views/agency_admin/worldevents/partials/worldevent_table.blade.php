<div class="col-md-10">
	<div class="panel panel-default">
		<div class="panel-heading">Agentes</div>
		

		<div class="panel-body">
			<table class="table table-bordered">
	            <tbody>
		            <tr>
		              <th>Date</th>
		              <th>Name</th>
		              <th>Story</th>				              
		              <th>Public</th>
		              <th>Description</th>
		            </tr>
	            @foreach($worldevents as $event)
	            
	            <tr>
	              <td>{{$event->date}}</td>
	              <td><a href="{{$event->url}}" target="_blank" rel="noreferrer noopener">{{$event->name}}</a></td>
	              <td>
	                	{{$event->story}}
	              </td>
	              <td>
	              	<form action="{{route('worldevents.visibility')}}" method="POST">
	              		<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
	              		<input type="hidden" name="id" value="{{$event->id}}">
	              		<select name="public" id="public" name="public">
	              			@if($event->public)
	              				<option value="1">Public</option>
	              				<option value="0">Hidden</option>
	              			@else
								<option value="0">Hidden</option>
								<option value="1">Public</option>
	              			@endif
	              		</select>
	              		<input class="btn btn-tiny" type="submit" value="update">
	              	</form>
	              	              	
	              </td>
	              <td>{{$event->description}}</td>
	              
	            </tr>
	            @endforeach
	            
	          </tbody>
			</table>
		</div>
	</div>
</div>
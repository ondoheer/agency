<div class="panel panel-default">
	<div class="panel-heading">Register Event</div>
	

	<div class="panel-body">
		
		<form method="POST" action="{{route('worldevents.store')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="name">
				Name
			</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="date">
				Date
			</label>
			<input class="form-control" type="date" id="date" name="date">
			<label for="story">
				Event Story
			</label>
			<input class="form-control" type="text" id="story" name="story">
			<label for="url">
				Event URL (in case there is a website)
			</label>
			<input class="form-control" type="url" id="url" name="url">
			
			<label for="description">description</label>
			<textarea class="form-control" name="description" id="description" cols="30" rows="10"></textarea>

			<input type="submit" class="btn btn-success">

		</form>
	</div>
</div>
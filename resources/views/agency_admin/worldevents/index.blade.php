@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	
<div class="row">
	
	<div class="col-md-8">
		@include('agency_admin.worldevents.partials.worldevent_table')
		
	</div>
	<div class="col-md-4">
		@include('agency_admin.worldevents.partials.worldevent_form')
	</div>
	
	
</div>

@endsection
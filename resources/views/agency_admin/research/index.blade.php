@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection
@section('main-content')
	
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">Create Research
				<h5>Remaining points: {{$agency->research_points}}</h5>
			</div>
			
			

			<div class="panel-body">
				<form action="{{route('research.store')}}" method="POST">
					<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">

					<label for="name">Name</label>
					<input class="form-control" type="text" name="name", id="name">
					<label for="slug">Slug</label>
					<input class="form-control" type="text" name="slug", id="slug">
					<label for="required_time">Required Time (days)</label>
					<input class="form-control" type="integer" name="required_time", id="required_time">
					<label for="points">Cost in points</label>
					<input class="form-control" type="integer" name="points", id="points">
					<label for="lab_size">Minimum Required Lab</label>
					<select class="form-control" name="lab_size" id="lab_size">
						@foreach($labs as $lab)
							<option value="{{$lab->id}}">{{$lab->name}}</option>
						@endforeach
					</select>
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" cols="30" rows="10"></textarea>
					<input class="btn btn-success" type="submit">
					
					
				</form>

			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">Researches</div>
			

			<div class="panel-body">
				<table class="table table-bordered">
		            <tbody><tr>
		              <th style="width: 10px">#</th>
		              <th>Name</th>
		              <th>Research Time</th>
		              <th>Laboratory</th>
		              <th>% Completed</th>
		            </tr>
		            @foreach($researches as $research)
		            <tr>
		              <td>{{$research->id}}</td>
		              <td>{{$research->name}}</td>
		              <td>
		                	{{$research->required_time}} days
		              </td>
		              <td>
		              	{{$research->laboratory->name or "Not started yet"}}		              	
		              </td>
		              <td>
  		                <div class="progress sm" style="height:20px">
  		                	
  	                      <div class="progress-bar progress-bar-aqua" style="width: {{$research->percentage_completed}}%; ">
  	                      	{{$research->percentage_completed}} %
  	                      </div>
  	                    </div>
		              </td>
		            </tr>
		            @endforeach
		            
		          </tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>
				
@endsection
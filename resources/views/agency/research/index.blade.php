@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection
@section('main-content')
	
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">On Going Researches</div>
			

			<div class="panel-body">
			@foreach($researches as $research)
				@if($research->laboratory)
				<div class="row">
					<div class="col-md-12">
						<div class="box box-default collapsed-box">
				            <div class="box-header with-border">
								<h3 class="box-title">{{$research->name}} - {{$research->required_time}} days - {{$research->laboratory->name}} - {{$research->daysLeft()}} days left</h3>
							  <div class="box-tools pull-right">
							    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
							    </button>
							  </div>
							  <div class="progress-group">
						  		<div class="progress sm">
						  		  <div class="progress-bar progress-bar-aqua" style="width: {{$research->percentage_completed}}%"></div>
						  		</div>
						  	  </div>	
							  <!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="display: none;">
							  <p><strong>Details:</strong> {{$research->description}}</p>
							 </div>
						</div>
					</div>
				</div>

				
				@endif	
			@endforeach
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Avaliable Researches - Remaining Points: <strong>{{$agency->research_points}}</strong></div>
			

			<div class="panel-body">
			@foreach($researches as $research)
				@if(!$research->laboratory)
				<div class="row">
					<div class="col-md-12">
						<div class="box box-default collapsed-box">
				            <div class="box-header with-border">
				              <h3 class="box-title">{{$research->name}} - {{$research->required_time}} days - Cost {{$research->points}} pts.</h3>

				              <div class="box-tools pull-right">
				                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
				                </button>
				              </div>
				              <!-- /.box-tools -->
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body" style="display: none;">
				              <p><strong>Details:</strong> {{$research->description}}</p>
				              <hr>
				              <div class="col-md-7">
				              	@if($availiable_labs->count())
					              	<form method="POST" action="{{route('research.start', $research->id)}}" >
					              		<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
					              		<input type="hidden" value="{{$research->id}}">
					              		<label for="lab">Choose Lab</label>
					              		<select class="form-control"name="lab" id="lab">
					              			@foreach($availiable_labs as $lab)
												<option value="{{$lab->id}}">{{$lab->name}}</option>
					              			@endforeach
					              		</select>
					              		<input type="submit" class="btn btn-xs btn-success" value="Start Project">
					              	</form>
					              @else
									<p>No availiable labs</p>
					              @endif
				              </div>
				            </div>
				            <!-- /.box-body -->
				          </div>
						
					</div>
				</div>		
				
				@endif	
			@endforeach
			</div>
		</div>
	</div>
	
</div>


		
@endsection
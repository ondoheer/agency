@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	
	@include('agency.partials.offices_summary_data')

	<div class="row">
		<div class="col-md-2">
			@include('agency.partials.office_create_form')
		</div>
		@if($offices_count > 0)
			<div class="col-md-3">
				@include('agency.partials.infrastructure_create_form')
			</div>		
			
			<div class="col-md-3 ">
				@include('agency.partials.laboratories_create_form')
			</div>
		@endif
	</div>
	@if($offices_count > 0)
	<div class="row">
		<div class="col-md-3">
			@include('agency.partials.offices_list')
		</div>
		<div class="col-md-4 ">
			@include('agency.partials.infrastructures_list')
		</div>
		<div class="col-md-4 ">
			@include('agency.partials.labs_list')
		</div>
	</div>
	@endif

			
@endsection
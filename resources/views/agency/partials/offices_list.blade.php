<div class="panel panel-default">
	<div class="panel-heading">Offices/Bases</div>
	

	<div class="panel-body">
		<table class="table table-bordered">
            <tbody><tr>              
              <th>Office</th>
              <th>Location</th>
              <th >Size</th>
              
            </tr>
            @foreach($offices as $office)
            <tr>
              <td>{{$office->name}}</td>
              <td>{{$office->location}}</td>
              <td>{{$office->size->name}}</td>
             
              
            </tr>
            @endforeach
            
          </tbody>
		</table>
	</div>
</div>
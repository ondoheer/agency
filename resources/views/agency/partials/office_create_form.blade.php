<div class="panel panel-default">
	<div class="panel-heading">Buy New Office
		<h5>Remaining points: {{$agency->infrastructure_points}}</h5>
	</div>
	
	

	<div class="panel-body">
		<form method="POST"action="{{route('office.create')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="name">
				Name
			</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="location">
				location
			</label>
			<input class="form-control" type="text" id="location" name="location">
			
			<label  for="type">Office Type</label>
			<select class="form-control" name="type" id="type">
				@foreach($base_type as $type)
					<option value="{{$type->id}}">{{$type->name}} (cost: {{$type->points}}pts)</option>
				@endforeach
			</select>

			<input type="submit" class="btn btn-success">
		</form>
	</div>
</div>
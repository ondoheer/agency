<div class="panel panel-default">
	<div class="panel-heading">Register Mission</div>
	

	<div class="panel-body">
		
		<form method="POST" action="{{route('mission.store')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="name">
				Name
			</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="start_date">
				Start Date
			</label>
			<input class="form-control" type="date" id="start_date" name="start_date">
			<label for="end_date">
				End Date
			</label>
			<input class="form-control" type="date" id="end_date" name="end_date">
			<label for="objective">
				Objective
			</label>
			<input class="form-control" type="text" id="objective" name="objective">
			<label for="report">Report</label>
			<textarea class="form-control" name="report" id="report" cols="30" rows="10"></textarea>

			<input type="submit" class="btn btn-success">

		</form>
	</div>
</div>
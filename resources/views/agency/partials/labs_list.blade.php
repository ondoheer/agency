<div class="panel panel-default">
	<div class="panel-heading">Labs</div>
	

	<div class="panel-body">
		<table class="table table-bordered">
            <tbody><tr>              
              <th>lab</th>
              <th>Location</th>
              <th >Researching</th>
              <th > Research %</th>
            </tr>

            @foreach($labs as $lab)
            <tr>  
              <td>{{$lab->name}}</td>
              <td>{{$lab->office->name}}</td>
              <td>{{$lab->research or "no research"}}</td>
              <td>{{$lab->research->percentage_completed or "-"}} %</td>
              
            </tr>
            @endforeach
            
          </tbody>
		</table>
	</div>
</div>
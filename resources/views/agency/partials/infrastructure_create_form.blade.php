<div class="panel panel-default">
	<div class="panel-heading">Build new Infrastructure
		<h5>Remaining points: {{$agency->infrastructure_points}}</h5>
	</div>
	<div class="panel-body">
	@if(!$infrastructures)
	<p>You have to build an office before you can build infrastructures</p>
	@else
	

		<form method="POST"action="{{route('infrastructure.create')}}">
			<label for="name">
				Name
			</label>	
			<input class="form-control" type="text" id="name" name="name">
			
			<label  for="type">Infrastructure Type</label>
			<select class="form-control" name="type" id="type">
				@foreach($infrastructure_types as $inf)
					<option value="{{$inf->id}}">{{$inf->name}} (cost: {{$inf->points}}pts)</option>
				@endforeach
			</select>		
	
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="office">
				For Office/Base
			</label>		
			<select class="form-control" name="office" id="office">
				@foreach($offices as $office)
					<option value="{{$office->id}}">{{$office->name}}</option>
				@endforeach
			</select>
			
			

			<input type="submit" class="btn btn-success">
		</form>
		@endif
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Agents</div>
	

	<div class="panel-body">
		<table class="table table-bordered">
            <tbody><tr>
              <th style="width: 10px">#</th>
              <th>Agent</th>
              <th>Type</th>
              <th style="width: 40px">Player</th>
            </tr>
            @foreach($agents as $agent)
            <tr>
              <td>{{$agent->id}}</td>
              <td>{{$agent->name}}</td>
              <td>
                {{$agent->type->name}}
              </td>
              <td>
              	@if($agent->player->email == "ondoheer@gmail.com")
                <span class="badge bg-yellow">NPC</span>
              	
              	@else
				        <span class="badge bg-green">{{$agent->player->name}}</span>
              	@endif
              </td>
            </tr>
            @endforeach
            
          </tbody>
		</table>
	</div>
</div>
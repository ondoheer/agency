<div class="panel panel-default">
	<div class="panel-heading">Agency Stats</div>
	

	<div class="panel-body">
		<table class="table table-bordered">
            <tbody><tr>
              <th ">Stat</th>
              <th>Total pts.</th>
              <th>Left pts.</th>
              
            </tr>
            
            <tr>
              <td>Agents</td>
              <td>{{$agent_total_points}}</td>
              <td>{{$agency->agent_points}}</td>
            </tr>
            <tr>
              <td>Infrastructure</td>
              <td>{{$infrastructure_total_points}}</td>
              <td>{{$agency->infrastructure_points}}</td>
            </tr>
            <tr>
              <td>Research</td>
              <td>      </td>
              <td>{{$agency->research_points}}</td>
            </tr>
                 
            <tr>
              <td>Information</td>
              <td></td>
              <td>{{$agency->information_points}}</td>
            </tr>
           
            
          </tbody>
		</table>
	</div>
</div>
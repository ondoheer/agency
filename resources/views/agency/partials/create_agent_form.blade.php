<div class="panel panel-default">
	<div class="panel-heading">Create your Agent</div>
	

	<div class="panel-body">
		
		<form method="POST" action="{{route('create_agent')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="name">
				Name
			</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="occupation">
				Occupation
			</label>
			<input class="form-control" type="text" id="occupation" name="occupation">
			<label for="birthdate">
				Birthdate
			</label>
			<input class="form-control" type="date" id="birthdate" name="birthdate">

			<label  for="type">Type</label>
			<select class="form-control" name="type" id="type">
				@foreach($agent_types as $type)
					<option value="{{$type->id}}">{{$type->name}}</option>
				@endforeach
			</select>
			<label  for="status">Status</label>
			<select class="form-control" name="status" id="status">
				@foreach($agent_status as $status)
					<option value="{{$status->id}}">{{$status->name}}</option>
				@endforeach
			</select>

			<input type="submit" class="btn btn-success">

		</form>
	</div>
</div>
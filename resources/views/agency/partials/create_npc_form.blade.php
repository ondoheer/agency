<div class="panel panel-default">
	<div class="panel-heading">Create NPC</div>
	

	<div class="panel-body">
		
		<form method="POST" action="{{route('npc.store')}}">
			<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="name">
				Name
			</label>
			<input class="form-control" type="text" id="name" name="name">
			<label for="location">
				location
			</label>
			<input class="form-control" type="text" id="location" name="location">
			<label for="description">Description</label>
			<textarea class="form-control" name="description" id="desription" cols="30" rows="10"></textarea>
			
			<input type="submit" class="btn btn-success">

		</form>
	</div>
</div>
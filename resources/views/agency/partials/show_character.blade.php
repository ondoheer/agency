<div class="panel panel-default">
		<div class="panel-heading">Your Current Agent</div>
		
		@if($user->agent)

		<div class="panel-body">
			<h3>{{$user->agent->name}}</h3>
			<h4>{{$user->agent->occupation}}</h4>
			<h5>{{$user->agent->type->name}}</h5>
		</div>

		@endif
</div>
<div class="panel panel-default">
	<div class="panel-heading">Infrastructures</div>
	

	<div class="panel-body">
		<table class="table table-bordered">
            <tbody><tr>              
              <th>Infrastructure</th>
              <th>Location</th>
              <th>Type</th>
              
            </tr>

            @foreach($infrastructures as $infrastructure)
            <tr>  
              <td>{{$infrastructure->name}}</td>
              <td>{{$infrastructure->office->name}}</td>
              <td>{{$infrastructure->type->name}}</td>
              
              
            </tr>
            @endforeach
            
          </tbody>
		</table>
	</div>
</div>
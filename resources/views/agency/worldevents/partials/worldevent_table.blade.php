<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">Agentes</div>
		

		<div class="panel-body">
			<table class="table table-bordered">
	            <tbody>
		            <tr>
		              <th>Date</th>
		              <th>Name</th>
		              <th>Story</th>				              
		              
		              <th>Description</th>
		            </tr>
	            @foreach($worldevents as $event)
	            
	            <tr>
	              <td>{{$event->date}}</td>
	              <td><a href="{{$event->url}}" target="_blank" rel="noreferrer noopener">{{$event->name}}</a></td>
	              <td>
	                	{{$event->story}}
	              </td>
	              
	              <td>{{$event->description}}</td>
	              
	            </tr>
	            @endforeach
	            
	          </tbody>
			</table>
		</div>
	</div>
</div>
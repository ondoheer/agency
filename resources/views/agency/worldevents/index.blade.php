@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	
<div class="row">
	
	@include('agency.worldevents.partials.worldevent_table')
	
	
	
</div>

@endsection
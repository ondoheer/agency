@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')	

	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">npcs</div>
				

				<div class="panel-body">
					<table class="table table-bordered">
			            <tbody><tr>
			              <th style="width: 10px">#</th>
			              <th>Name</th>
			              <th>Location</th>
			              <th>Description</th>
			              
			            </tr>
			            @foreach($npcs as $npc)
			            <tr>
			              <td>{{$npc->id}}</td>
			              <td>{{$npc->name}}</td>
			              <td>
			                	{{$npc->location}}
			              </td>
			              <td>
			              	{{$npc->description}}		              	
			              </td>
			              
			            </tr>
			            @endforeach
			            
			          </tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			@include('agency.partials.create_npc_form')
		</div>
	</div>

			
@endsection
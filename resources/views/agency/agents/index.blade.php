@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	@include('agency.partials.agents_summary_data')
	

	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">Agentes</div>
				

				<div class="panel-body">
					<table class="table table-bordered">
			            <tbody><tr>
			              <th style="width: 10px">#</th>
			              <th>Name</th>
			              <th>Occupation</th>
			              <th>Age</th>
			              <th>Type</th>
			              <th>Status</th>
			              <th>Player</th>
			            </tr>
			            @foreach($agents as $agent)
			            <tr>
			              <td>{{$agent->id}}</td>
			              <td>{{$agent->name}}</td>
			              <td>
			                	{{$agent->occupation}}
			              </td>
			              <td>
			              	{{$agent->age}}		              	
			              </td>
			              <td>{{$agent->type->name}}</td>
			              <td>{{$agent->status->name or 'None'}}</td>
			              <td>{{$agent->player->name}}</td>
			            </tr>
			            @endforeach
			            
			          </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

			
@endsection
@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')
	
	
	@include("agency.partials.agency_stats_big")
	

	<div class="row">
		
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					@include('agency.partials.offices_list')
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ">
					@include('agency.partials.agents_list')
				</div>
			</div>
			
		</div>

		
		@if(Auth::user()->agent && !Auth::user()->agent->birthdate)
		<div class="col-md-4 ">
			<form method="POST" action="{{route('patch_date')}}">

				<input class="form-control" type="hidden" name="_token" value="{{ csrf_token() }}">
				
				<span></span>
				<input type="hidden" name="agent_id" value="{{Auth::user()->agent->id}}">
				<label for="birthdate">Update your agent {{Auth::user()->agent->name}} birthdate</label>
				<input class="form-control" value="0" type="date" name="birthdate", id="birthdate">
				<br>
				<input class="btn btn-success" type="submit" value="Update">
			</form>
		</div>
		@else
		
		<div class="col-md-4 ">

			@if(Auth::user()->agent && Auth::user()->email != "ondoheer@gmail.com")
				
				@include('agency.partials.show_character')
				
			@else
				@include('agency.partials.create_agent_form')
			@endif
		</div>
		@endif
	</div>
	

			
@endsection
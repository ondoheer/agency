@extends('layouts.app')

@section('htmlheader_title')
	Admin
@endsection

@section('contentheader_title')
	Panel de Administrador
@endsection

@section('contentheader_description')
	Panel de administración
@endsection

@section('main-content')	

	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">Missions</div>
				

				<div class="panel-body">
					<table class="table table-bordered">
			            <tbody><tr>
			              <th style="width: 10px">#</th>
			              <th>Start date</th>
			              <th>End date</th>
			              <th>Name</th>
			              <th>Objective</th>
			              <th>Report</th>
			              
			            </tr>
			            @foreach($missions as $mission)
			            <tr>
			              <td>{{$mission->id}}</td>
			              <td>{{$mission->start_date}}</td>
			              <td>{{$mission->end_date}}</td>
			              <td>{{$mission->name}}</td>
			              <td>
			                	{{$mission->objective}}
			              </td>
			              <td>
			              	{{$mission->report}}		              	
			              </td>
			              
			            </tr>
			            @endforeach
			            
			          </tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			@include('agency.partials.create_mission_form')
		</div>
	</div>

			
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NPC extends Model
{
    protected $table = 'npcs';

    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentStatus extends Model
{
    protected $table = 'agent_status';

    public function agents()
    {
    	return $this->hasMany(Agent::class);
    }
}

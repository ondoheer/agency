<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
	public $timestamps = false;

    function laboratory()
    {
    	return $this->hasOne(Infrastructure::class, 'id', 'lab_id');
    }

    function requiredLab()
    {
    	return $this->belongsTo(InfrastructureType::class);
    }

    public static function ongoing()
    {
        return Research::whereNotNUll('lab_id')->get();
    }

    public function daysLeft(){
        return $this->required_time - $this->passed_time;
    }

    public static function busyLabs()
    {
        $ongoing_researches = Research::whereNotNUll('lab_id')->get();
        $busy_labs = [];
        foreach ($ongoing_researches as $res) {
             array_push($busy_labs, $res->lab_id);
             $busy_labs = array_unique($busy_labs);
        } 

        // Labs with no current ongoing research
        $labs = Infrastructure::select('id', 'name')->whereIn('infrastructure_type_id', [12,13,14])->whereIn('id', $busy_labs)->get();
        
        // we return a falsy value
        if(!$labs)
        {
            return $labs;
        }
        
        return $labs;
    }

    /*
		Here we show which labs could perform the required research
    */
    public static function availiableLabs()
    {
        
        
        $ongoing_researches = Research::whereNotNUll('lab_id')->get();
        $busy_labs = [];
        foreach ($ongoing_researches as $res) {
             array_push($busy_labs, $res->lab_id);
             $busy_labs = array_unique($busy_labs);
        } 

        // Labs with no current ongoing research
        $labs = Infrastructure::select('id', 'name')->whereIn('infrastructure_type_id', [12,13,14])->whereNotIn('id', $busy_labs)->get();
        
        // we return a falsy value
        if(!$labs)
        {
            return $labs;
        }
        
        return $labs;
    }

    public function calcultatePercentage(int $current,int $total)
    {
        $percentage = ($current/$total) * 100;
        if($percentage > 100)
        {
            return 100;
        } else {
            return $percentage;
        }
    }
}

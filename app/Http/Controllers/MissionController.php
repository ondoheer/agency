<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Mission;
use App\Agency;

class MissionController extends Controller
{
    public function show()
    {
    	$missions = Mission::orderBy('start_date')->get();
        $agency = Agency::first();
    	return view('agency.missions.index', compact('missions', 'agency'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name"=>"required",
            "objective"=>"required",
            "report"=>"required",
            "end_date"=>"required",
            "start_date"=>"required"
        ]);


    	$mission = new Mission();
    	$mission->name = $request->name;
    	$mission->objective = $request->objective;
    	$mission->report = $request->report;
        $mission->start_date = $request->start_date;
        $mission->end_date = $request->end_date;
    	$mission->save();

        $request->session()->flash("status", "Mission created");

    	return back();
    }
}

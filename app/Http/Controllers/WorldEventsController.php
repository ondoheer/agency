<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\WorldEvent;
use App\Agency;

class WorldEventsController extends Controller
{
    
	public function publicShow()
    {
    	$agency = Agency::first();
    	$worldevents = WorldEvent::where('public', '=', 1)->orderBy('date')->get();
    	return view('agency.worldevents.index', compact(
    		'worldevents',
    		'agency'
    	));
    }

    public function show()
    {
    	$agency = Agency::first();
    	$worldevents = WorldEvent::orderBy('date')->get();
    	return view('agency_admin.worldevents.index', compact(
    		'worldevents',
    		'agency'
    	));
    }

    public function store(Request $request)		
    {
    	$this->validate($request, [
    		'name'=>'required',
    		'date'=>'required',
    		'url'=>'url',
    		'description'=>'required',

    	]);

    	$event = new WorldEvent();
    	$event->name = $request->name;
    	$event->date = $request->date;
    	if($request->url){
    		$event->url = $request->url;
    	}
    	if($request->story){
    		$event->story = $request->story;
    	}
    	$event->description = $request->description;
    	$event->save();

    	$request->session()->flash("status", "Event created");

    	return back();


    }

    public function setVisibility(Request $request)
    {
        $this->validate($request, [
            'id'=> 'required',
            'public'=>'required|max:1|min:0'
        ]);
        $event = WorldEvent::find($request->id);
        $event->public = $request->public;
        $event->save();

        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use App\User;
use App\Agent;
use App\AgentType;
use App\Agency;
use App\Research;
use App\WorldEvent;


class AgencyAdminController extends Controller
{
    public function show()
    {

    	$users = User::all();
    	$agents = Agent::all();
        $agency = Agency::first();
      
    	$agent_types = AgentType::all();
    	return view('agency_admin.index',
    		compact('users',
    			'agents',
    			'agent_types',
                'agency'
    		));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'foundation'=>'required|integer'
        ]);



        $agency = new Agency;
        $agency->name = $request->name;
        $agency->foundation_year = $request->foundation;
        $agency->save();

        $request->session()->flash("status", "Agency created");

        return redirect(route('agency'));
    }

    

    public function assignUserToAgent(Request $request)
    {   	
    	
    	$agent = Agent::find($request->agent);
    	$agent->user_id = $request->user;
    	$agent->save();
    	return redirect(route('agents_admin'));
    }

    public function manageAgency()
    {
        
        $agency = Agency::first();
        
        
        return view('agency_admin.agency.index', compact('agency'));
    }

    public function setStartDate(Request $request)
    {
        $this->validate($request, [
            "start_date"=>'required|date'
        ]);

        $agency = Agency::first();
        $agency->starting_date = $request->start_date;
        if(!$agency->current_date){
            $agency->current_date = $agency->starting_date;
        }
        $agency->save();
        $request->session()->flash("status", "The agency starting date was set");

        return back();

    }

    public function updateCurrentDate(Request $request){
        
        $this->validate($request, [
            'days'=>'required'
        ]);
        $agency = Agency::first();
        $days = $request->days;
        
        $agency->previous_date = $agency->current_date;
        $agency->current_date = Carbon::createFromFormat('d-m-Y', $agency->previous_date)->addDays($days)->format('d-m-Y');
        
        // Excecute passage of time
        $agency->save();
        // Agents grow older
        $agents = Agent::select('id', 'age', 'birthdate')->where("agency_id", '=', $agency->id)->get();
       
        foreach ($agents as $agent) {
            $current_age = Carbon::createFromFormat('d-m-Y', $agent->birthdate)->diffInYears(
                 Carbon::createFromFormat('d-m-Y',$agency->current_date));

            if($current_age > $agent->age)
            {
                $agent->age = $current_age;
                $agent->save();
                // TODO send bday mail
                                
            }           
            

        }  
        
        // Researches advance
        $ongoing = Research::ongoing();
        foreach ($ongoing as $research) {
            $research->passed_time += $days;

            $research->percentage_completed = $research->calcultatePercentage($research->passed_time, $research->required_time);
            $research->save();
        }
        // Events become public happen
        $worldevents = WorldEvent::select('id', 'public','date')
                            ->where('public', '=', 0)
                            ->whereDate('date', '<', Carbon::createFromFormat('d-m-Y',$agency->current_date))
                            ->get();
        foreach ($worldevents as $event) {
            $event->public = 1;
            $event->save();
        }
       

        $request->session()->flash("status", "The agency date was updated, so, researches and agent ages as well");
        
        
        return back();
    }

    public function updateAgencyPoints(Request $request)
    {
        
        $this->validate($request, [
            "research_points"=>"min:0|numeric",
            "infrastructure_points"=>"min:0|numeric",
            "agent_points"=>"min:0|numeric",
            "information_points"=>"min:0|numeric"
        ]);
        
        $agency = Agency::first();
        $agency->research_points = $request->research_points;
        $agency->infrastructure_points = $request->infrastructure_points;
        $agency->agent_points = $request->agent_points;
        $agency->information_points = $request->information_points;
        

        $agency->save();
        $request->session()->flash("status", "The agency stats were updated");

        return back();
    }
}

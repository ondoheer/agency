<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Infrastructure;
use App\InfrastructureType;
use App\Agency;

class InfrastructureController extends Controller
{
    public function store(Request $request)
    {

        $this->validate($request, [
            "name"=>"required"
        ]);
    	$infrastructure = new Infrastructure();
    	
    	$infrastructure->name = $request->name;
    	$infrastructure->office_id = $request->office;
    	$infrastructure->infrastructure_type_id = $request->type;
    	$infrastructure->save();

    	$agency = Agency::first();
    	$infrastructure_type = InfrastructureType::find($infrastructure->infrastructure_type_id);
    	$agency->infrastructure_points -= $infrastructure_type->points;
    	$agency->save();

        $request->session()->flash("status", "Infrastructure created");

    	return back();
    }
}

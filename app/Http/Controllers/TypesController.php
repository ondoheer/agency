<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgentType;
use App\InfrastructureType;
use App\Agent;
use App\Http\Requests;
use App\Agency;

class TypesController extends Controller
{
    public function show()
    {
    	$agents = AgentType::all();
        $agency = Agency::first();
        $infrastructures = InfrastructureType::all();
        // dd(compact('agents', 'laboratories', 'infrastructures'));

    	return view('agency_admin.types.index', compact('agents',  'infrastructures', 'agency'));
    }

    public function createType(Request $request)
    {
    	$this->validate($request, [
    		'name'=>'required',    		
    		'points'=>'required|max:50|min:1',
            'type'=>'required'
    	]);

    	

        switch ($request->type) {
            case 'agent':
                $type = new AgentType;
                break;            
            case 'laboratory':
                $type = new LaboratoryType;
                break;
            case 'infrastructure':
                $type = new InfrastructureType;
        }
    	$type->name = $request->name;
    	$type->slug = str_replace(" ","-", strtolower($request->name));
    	$type->points = $request->points;
    	$type->description = $request->description;
    	$type->save();

    	$request->session()->flash("status", "Type created");
    	return redirect('types');
    }

    
}

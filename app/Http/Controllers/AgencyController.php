<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Agency;
use App\Agent;
use App\AgentType;
use App\Infrastructure;
use App\Laboratory;
use App\Research;
use App\Vehicle;
use App\Office;
use App\User;
use App\AgentStatus;

class AgencyController extends Controller
{
    public function show(Request $request)
    {
    	$agency = Agency::first();
        
        $offices = Office::with('size')->get();
        $agents = Agent::with(['type',
                             'player'=>function($query){
                                $query->orderBy('id', 'desc');
                    }])->get();
        $agent_types = AgentType::all();
        
        $user = $request->user();
        $agent_status = AgentStatus::all();
        if($agency){
            $agent_total_points = $agency->agents->sum('points'); 
            $infrastructure_total_points = 0;
            foreach($offices as $office) {
                $infrastructure_total_points += $office->size->points;
            }
            
            $research_total_points = Research::whereNotNUll('lab_id')->get()->sum('points');


            
        } else {
            $agent_total_points = 0;
            $infrastructure_total_points = 0;
        }
           

  


    	return view('agency.index', compact(
            'agency',
            'offices',
            'agents',            
            'agent_types', 
            'agent_status',           
            'user',
            'agent_total_points',
            'infrastructure_total_points',
            'research_total_points'

        ));
    }

    

    

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Research;
use App\Agency;
use App\Infrastructure;
use App\InfrastructureType;

class ResearchController extends Controller
{
    public function show()
    {
    	$researches = Research::all();
    	$agency = Agency::first();
        $availiable_labs = Research::availiableLabs();
        $busy_labs = Research::busyLabs();
    	return view('agency.research.index', compact(
    		'researches',
    		'agency',
            'availiable_labs',
            'busy_labs'
    	));
    }

    public function start(Request $request, Research $research)
    {
    	
        $agency = Agency::first();
        $research->lab_id = $request->lab;
        $research->save();
        $agency->research_points -= $research->points;
        $agency->save();

        // asign research to lab
        // deduct points from agency pool

        


    	return back();
    }

    public function finish()
    {
    	$agency = Agency::first();

    	return back();
    }

    public function create()
    {
    	$researches = Research::all();
    	$agency = Agency::first();
    	$labs = InfrastructureType::whereIn('id', [12,13,14])->get();
    	return view('agency_admin.research.index', compact(
    		'researches',
    		'agency',
    		'labs'
    	));
    }

    public function store(Request $request)
    {
    	
        $this->validate($request, [
            "name"=>"required",
            "description"=>"required",
            "required_time"=>"numeric|min:1",
            "points"=>"numeric|min:0"
        ]);
        $research = new Research();
    	$research->name = $request->name;
    	$research->slug = $request->slug;
    	$research->description = $request->description;
    	$research->required_time = $request->required_time;
        $research->points = $request->points;
    	$research->required_lab_id = $request->lab_size;

    	$research->save();

        $request->session()->flash("status", "Research created");

    	return back();
    }
}

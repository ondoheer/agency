<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;

use App\Agent;
use App\Agency;


class AgentController extends Controller
{
    

    public function patchDate(Request $request)
    {
        $this->validate($request, [
            'birthdate'=>'required'
        ]);
        $agency = Agency::first();
        $agent = Agent::find($request->agent_id);
        $agent->birthdate = $request->birthdate;
        $agency->previous_date = $agency->current_date;
        $agent->age = Carbon::createFromFormat('d-m-Y', $agent->birthdate)->diffInYears(
                 Carbon::createFromFormat('d-m-Y',$agency->current_date));
        $agent->save();
        
        $request->session()->flash("status", "Agent created");
        return back();



    }
    public function createAgent(Request $request)
    {

        $this->validate($request, [
            "name"=>'required',
            "age"=>'numeric|min:0',
            "occupation"=>"required",
            
        ]);

        $agency = Agency::first();
        $agent = new Agent();
        $agent->user_id = $request->user()->id;
        $agent->name = $request->name;
        $agent->occupation = $request->occupation;
        $agent->agent_type_id = $request->type;
        $agent->agency_id = Agency::first()->id; // TODO
        $agent->status_id = $request->status;
        if($request->birthdate)
        {
            $agent->birthdate = $request->birthdate;
            $agent->age = Carbon::createFromFormat('d-m-Y', $agent->birthdate)->diffInYears(
                 Carbon::createFromFormat('d-m-Y',$agency->current_date));
            
            
        }

        $agent->save();
        // this should be a read query not write write
        $agent->points = $agent->type->points;
        $agent->save();

        $request->session()->flash("status", "Agent created");
        return back();

    }

    public function show()
    {
        $agents = Agent::orderBy('status_id')->get();
        $agency = Agency::first();
        $field_agents = Agent::where('agent_type_id', '<',  '13')->get()->count();

        $researchers = Agent::where('agent_type_id', '>', '12')->where('agent_type_id', '<', '16')->get()->count();

        $specials = Agent::where('agent_type_id', '=',  '16')->get()->count();
        
        $agent_count = $agents->count();

        return view('agency.agents.index', compact(            
            'agency',
            'agents',  
            'field_agents',
            'researchers',
            'specials',
            'agent_count'           
        ));
        

        
    }
}

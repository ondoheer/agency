<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Office;
use App\InfrastructureType;
use App\Infrastructure;
use App\Agency;
use App\OfficeType;

class OfficeController extends Controller
{
    public function show()
    {
	    $agency = Agency::first();
        $offices = Office::all();
        $offices_count = $offices->count();
    	$comm_count = 3;
    	$imprisionment_count = 4;
    	
    	$base_type = OfficeType::where('points', '<=', $agency->infrastructure_points)->get();
        $lab_types = InfrastructureType::whereIn('id', [12,13,14])->where('points', '<=', $agency->infrastructure_points)->get();
        $labs = Infrastructure::whereIn('infrastructure_type_id', [12,13,14])->get();
        $infrastructure_types = InfrastructureType::whereNotIn('id', [12,13,14])->where('points', '<=', $agency->infrastructure_points)->get();
        $infrastructures = Infrastructure::whereNotIn('infrastructure_type_id', [12,13,14])->get();
    	return view('agency.office.index',compact(
    		'offices_count',
            'offices',
    		'comm_count',
    		'imprisionment_count',    		
    		'base_type',
            'agency',
            'lab_types',
            'labs',
            'infrastructure_types',
            'infrastructures'
    	));
    }

    public function create(Request $request)
    {
    	
        $this->validate($request,[
            "name"=>"required",

        ]);
        $agency = Agency::first();
        $office = new Office();

    	$office->name = $request->name;
    	$office->location = $request->location;
        $office->office_type_id = $request->type;

        $office->agency_id = $agency->id;
        $office->save();
       
        $agency->infrastructure_points -= $office->size->points;
        $agency->save();

        $request->session()->flash("status", "Office created");

        return back();
    	

    }
}

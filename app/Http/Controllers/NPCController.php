<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\NPC;
use App\Agency;

class NPCController extends Controller
{
    public function show()
    {
    	$npcs = NPC::all();
        $agency = Agency::first();
    	return view('agency.npcs.index', compact('npcs', 'agency'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            "name"=>"required",
            "location"=>"required",
            "description"=>"required"
        ]);
    	$npc = new NPC();
    	$npc->name = $request->name;
    	$npc->location = $request->location;
    	$npc->description = $request->description;
    	$npc->save();

        $request->session()->flash("status", "NPC created");

    	return back();
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Agency;
class ExistsAgency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Agency::first()) {
            $request->session()->flash('status', 'Your need to create an agency');
            return redirect()->route('agency.manage');
        }
        return $next($request);
    }
}

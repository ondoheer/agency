<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', function () {
	    return redirect('agency');
	});
});



/****************/
/*TEMP ROUTES*/
/***************/
Route::group(['prefix'=>'temp', 'middleware'=>['auth', 'existsagency']], function(){
	Route::post('/', 'AgentController@patchDate')->name('patch_date');
	
	
	
});

/****************/
/*AGENCY ROUTES*/
/***************/
Route::group(['prefix'=>'agency', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'AgencyController@show')->name('agency');
	
	
	
});

/************
Agent Routes
************/

Route::group(['prefix'=>'agent', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/list', 'AgentController@show')->name('manage_agents');
	Route::post('/', 'AgentController@createAgent')->name('create_agent');

});

/************************************
Office & Infrastructure Routes
************************************/
Route::group(['prefix'=>'office', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'OfficeController@show')->name('offices');
	Route::post('/', 'OfficeController@create')->name('office.create');
	Route::post('/infrastructure', 'InfrastructureController@store')->name('infrastructure.create');
});

/************************************
Research Routes
************************************/
Route::group(['prefix'=>'research', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'ResearchController@show')->name('researches');
	Route::post('/{research}', 'ResearchController@start')->name('research.start');
	
});

/************************************
Information Routes
************************************/
Route::group(['prefix'=>'information', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'informationController@publicShow')->name('public_information');
	Route::post('/use', 'informationController@use')->name('information.use');
	
});

/*********************************
Mission Routes
*********************************/
Route::group(['prefix'=>'mission', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'MissionController@show')->name('missions');
	Route::post('/', 'MissionController@store')->name('mission.store');
});

/*********************************
NPCs Routes
*********************************/
Route::group(['prefix'=>'npc', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'NPCController@show')->name('npcs');
	Route::post('/', 'NPCController@store')->name('npc.store');
});
/*********************************
World Events Routes
*********************************/
Route::group(['prefix'=>'worldevents', 'middleware'=>['auth', 'existsagency']], function(){
	Route::get('/', 'WorldEventsController@publicShow')->name('public_worldevents');

});	


/*************
Admin only
**************/
Route::group(['prefix'=>'admin', 'middleware'=>['auth']], function(){
	// create types
	Route::get('/types', 'TypesController@show')->name('types');
	// Manage a new agency
	Route::get('/agency/manage', 'AgencyAdminController@manageAgency')->name('agency.manage');
	// create agency
	Route::post('/agency', 'AgencyAdminController@store')->name('agency.store');
	// Set Agency Startingdate
	Route::post('/agency/start_date', 'AgencyAdminController@setStartDate')->name('agency.start_date');
	// Update Game current date
	Route::post('/agency/update_date', 'AgencyAdminController@updateCurrentDate')->name('agency.update_date');
	// Add points to agency
	Route::post('/agency/update', 'AgencyAdminController@updateAgencyPoints')->name('agency.update_points');
	// Create new agent types 
	Route::post('/agent', 'TypesController@createType')->name('types.create');
	// Add Researches
	Route::get('/research','ResearchController@create')->name('research.create');
	Route::post('/research', 'ResearchController@store')->name('research.store');
	// Manage Agents
	Route::get('/agents', 'AgencyAdminController@show')->name('agents_admin');	
	Route::post('/agents/assign_user', 'AgencyAdminController@assignUserToAgent')->name('agent_to_user');
	// Manage World Events
	Route::get('/worldevents', 'WorldEventsController@show')->name('worldevents');
	Route::post('/worldevents', 'WorldEventsController@store')->name('worldevents.store');
	Route::post('/', 'WorldEventsController@setVisibility')->name('worldevents.visibility');
	// Manage information Data
	Route::get('/', 'informationController@show')->name('information');
	Route::post('/store', 'informationController@store')->name('information.store');
});



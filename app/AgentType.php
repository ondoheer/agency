<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentType extends Model
{
    public function agents()
    {
    	return $this->hasMany(Agent::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Mission extends Model
{
    public $timestamps = false;
    protected $dates = ['start_date', 'end_date'];

    public function setStartDateAttribute($value)
    {
        //dd($value);
        $this->attributes['start_date'] = Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');   
    }

    public function getStartDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');   
    }

    public function getEndDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agency extends Model
{
	//as seen on https://laracasts.com/discuss/channels/general-discussion/laravel-create-a-new-record-using-inputall-with-date-fields?page=1
    
    protected $dates = ['starting_date', 'current_date', 'previous_date'];

    public function agents()
    {
    	return $this->hasMany(Agent::class);
    }

    public function offices()
    {
    	return $this->hasMany(Office::class);
    }

    public function getStartingDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }
    // THIS WAS COMMENTED BECAUSE WE ONLY SET THE STARTING DATE ONCE
    // AND IT BROKE WHEN WE TRIED TO SAVE IT AS IT CAME FROM THE FORM
    // public function setStartingDateAttribute($value)
    // {
    //     $this->attributes['starting_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');   
    // }

    public function getCurrentDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }

    public function setCurrentDateAttribute($value)
    {
        $this->attributes['current_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');   
    }

    public function getPreviousDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }

    public function setPreviousDateAttribute($value)
    {
        $this->attributes['previous_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');   
    }
}

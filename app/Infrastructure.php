<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infrastructure extends Model
{
    public function office()
    {
    	return $this->belongsTo(Office::class, 'office_id');
    }

    public function type()
    {
    	return $this->hasOne(InfrastructureType::class, 'id','infrastructure_type_id');
    }

    public function research()
    {
    	return $this->hasOne(Research::class, 'id', 'lab_id');
    }

    
}

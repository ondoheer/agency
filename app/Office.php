<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function agency()
    {
    	return $this->belongsTo(Agency::class);
    }

    public function size()
    {
        return $this->belongsTo(OfficeType::class, 'office_type_id');
    }

    public function infrastructures()
    {
    	return $this->hasMany(Infrastructure::class);
    }    

    
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    public $timestamps = false;
    protected $dates = ['birthdate'];

    public function agency()
    {
    	return $this->belongsTo(Agency::class);
    }

    public function player()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function type()
    {
    	return $this->belongsTo(AgentType::class, 'agent_type_id');
    }

    public function status()
    {
        return $this->belongsTo(AgentStatus::class);
    }

    public function setBirthdateAttribute($value)
    {
        
        $this->attributes['birthdate'] = Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');   
    }

    public function getBirthdateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationAction extends Model
{
    public $timestamps = false;
}

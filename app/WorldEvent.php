<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WorldEvent extends Model
{
    public $timestamps = false;
    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        
        $this->attributes['date'] = Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');   
    }

    public function getDateAttribute($value)
    {
        if($value){
            return Carbon::parse($value)->format('d-m-Y');
        }
        
    }
}
